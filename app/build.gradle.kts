plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.jetbrains.kotlin.android)
    alias(libs.plugins.jetbrainsKotlinKsp)
    alias(libs.plugins.hiltPlugin)
}

android {
    namespace = "com.rakibhasan.bs_assessment"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.rakibhasan.bs_assessment"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
        flavorDimensions += "version"
    }

    productFlavors {
        create("prod") {
            dimension = "version"
            applicationIdSuffix = ".prod"
            versionCode = 1001
            versionName = "1.0.1001"
            buildConfigField("String", "API_ENDPOINT", "\"/prod\"")
        }
        create("dev") {
            dimension = "environment"
            applicationIdSuffix = ".dev"
            versionCode = 2001
            versionName = "1.0.2001"
            buildConfigField("String", "API_ENDPOINT", "\"/dev\"")
        }
    }

    buildTypes {
        debug {
            buildConfigField("String", "BASE_URL", "\"https://example.com/api/\"")
        }
        release {
            buildConfigField("String", "BASE_URL", "\"https://example.com/api/\"")
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    dependencies {

        implementation(libs.androidx.core.ktx)
        implementation(libs.androidx.appcompat)
        implementation(libs.androidx.activity)
        implementation(libs.androidx.constraintlayout)
        implementation(libs.material)

        //test
        testImplementation(libs.junit)
        androidTestImplementation(libs.androidx.junit)
        androidTestImplementation(libs.androidx.espresso.core)
        testImplementation(libs.coroutines.test)

        //hilt
        implementation(libs.hilt.android)
        ksp(libs.hilt.android.compiler)

        //retrofit
        implementation(libs.retrofit)
        implementation(libs.retrofit.converter.gson)
        implementation(libs.okhttp.logging.interceptor)

        //navigation component
        implementation(libs.navigation.fragment.ktx)
        implementation(libs.navigation.ui.ktx)

        //viewmodel
        implementation(libs.androidx.lifecycle)

        //coroutine
        implementation(libs.kotlinx.coroutines.core)
        implementation(libs.kotlinx.coroutines.android)

        //glide
        implementation(libs.glide)

        //Gson
        implementation(libs.gson)

        //Datastore
        implementation(libs.androidx.datastore)

    }
}

dependencies {

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.activity)
    implementation(libs.androidx.constraintlayout)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
}