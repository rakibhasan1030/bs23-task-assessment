package com.rakibhasan.bs_assessment.feature.book_details.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class BookDetailsViewModel @Inject constructor() : ViewModel() {

    private val _url = MutableLiveData<String>()
    val url: LiveData<String> get() = _url

    /**
     * Sets the URL value in the [_url] LiveData.
     *
     * @param value The URL to be set.
     */
    fun setUrl(value: String) {
        _url.value = value
    }

}