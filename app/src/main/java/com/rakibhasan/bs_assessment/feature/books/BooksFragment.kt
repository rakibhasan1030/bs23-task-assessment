package com.rakibhasan.bs_assessment.feature.books

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rakibhasan.bs_assessment.R
import com.rakibhasan.bs_assessment.databinding.FragmentBooksBinding
import com.rakibhasan.bs_assessment.feature.book_details.presentation.viewmodel.BookDetailsViewModel
import com.rakibhasan.bs_assessment.feature.books.data.remote.dto.BookResponseDto
import com.rakibhasan.bs_assessment.feature.books.presentation.adapters.BooksAdapter
import com.rakibhasan.bs_assessment.feature.books.presentation.viewmodel.BookState
import com.rakibhasan.bs_assessment.feature.books.presentation.viewmodel.BooksViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class BooksFragment : Fragment() {
    private var _binding: FragmentBooksBinding? = null
    private val binding get() = _binding!!
    private val booksViewModel: BooksViewModel by activityViewModels()
    private val bookDetailViewModel: BookDetailsViewModel by activityViewModels()
    private lateinit var adapter: BooksAdapter

    override fun onResume() {
        super.onResume()
        booksViewModel.getBooks()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentBooksBinding.inflate(inflater, container, false)
        observe()
        return binding.root
    }

    private fun observe() {
        observeBookState()
        observePostData()
    }

    private fun observeBookState() {
        booksViewModel.bookState.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { handelPostsState(it) }
            .launchIn(lifecycleScope)
    }

    /**
     * Handles the state of book data retrieval.
     *
     * @param postState The state of book data retrieval, represented by [BookState].
     *                  Possible states include:
     *                  - [BookState.ShowToast]: To show a toast message with a specific message.
     *                  - [BookState.IsLoading]: To handle the loading state of book data.
     *                  - [BookState.Init]: To indicate the initialization state.
     */
    private fun handelPostsState(postState: BookState) {
        when (postState) {
            is BookState.ShowToast -> {
                binding.noDataFoundTV.visibility = View.VISIBLE
                binding.noDataFoundTV.text = postState.message
            }
            is BookState.IsLoading -> handelLoading(postState.isLoading)
            is BookState.Init -> Unit
        }
    }

    private fun handelLoading(loading: Boolean) {
        if (loading) {
            binding.progressBar.visibility = View.VISIBLE
            binding.recyclerView.visibility = View.GONE
            binding.noDataFoundTV.visibility = View.GONE
        } else {
            binding.progressBar.visibility = View.GONE
            binding.recyclerView.visibility = View.VISIBLE
            binding.noDataFoundTV.visibility = View.GONE
        }
    }


    private fun observePostData() {
        booksViewModel.books.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { handelPostsData(it) }
            .launchIn(lifecycleScope)
    }

    /**
     * Handles the received data of books from the API response.
     *
     * @param postResponseDto The response DTO containing the books data.
     */
    private fun handelPostsData(postResponseDto: BookResponseDto) {
        val books = postResponseDto.books
        if (books?.isNotEmpty() == true) {
            binding.recyclerView.visibility = View.VISIBLE
            binding.noDataFoundTV.visibility = View.GONE
            adapter = BooksAdapter(books, requireActivity()) { clickedItem ->
                // Access the clicked item's data using `clickedItem`
                if (!clickedItem.url.isNullOrBlank()) {
                    findNavController().navigate(R.id.action_booksFragment_to_bookDetailsFragment)
                        .also {
                            bookDetailViewModel.setUrl(clickedItem.url)
                        }
                } else Toast.makeText(
                    requireContext(),
                    "Page not found. Please, try again.",
                    Toast.LENGTH_SHORT
                ).show()
            }
            binding.recyclerView.adapter = adapter
            binding.recyclerView.layoutManager = LinearLayoutManager(context)
            binding.recyclerView.setHasFixedSize(true)
        } else {
            binding.recyclerView.visibility = View.GONE
            binding.progressBar.visibility = View.GONE
            binding.noDataFoundTV.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}