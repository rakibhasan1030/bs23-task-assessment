package com.rakibhasan.bs_assessment.feature.books.data.remote.repositoryImpl


import com.rakibhasan.bs_assessment.core.base.BaseResult
import com.rakibhasan.bs_assessment.core.base.Failure
import com.rakibhasan.bs_assessment.feature.books.data.remote.BookRemoteSource
import com.rakibhasan.bs_assessment.feature.books.data.remote.dto.BookResponseDto
import com.rakibhasan.bs_assessment.feature.books.domain.repository.BookRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class BookRepositoryImpl constructor(
    private val bookRemoteSource: BookRemoteSource
) : BookRepository {
    override suspend fun getBooks(): Flow<BaseResult<BookResponseDto, Failure>> = flow { emit(bookRemoteSource.getBooks()) }
}