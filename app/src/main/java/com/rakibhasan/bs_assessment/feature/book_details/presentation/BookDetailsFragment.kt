package com.rakibhasan.bs_assessment.feature.book_details.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.rakibhasan.bs_assessment.databinding.FragmentBookDetailsBinding
import com.rakibhasan.bs_assessment.feature.book_details.presentation.viewmodel.BookDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookDetailsFragment : Fragment() {
    private var _binding: FragmentBookDetailsBinding? = null
    private val binding get() = _binding!!

    private val bookDetailViewModel: BookDetailsViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBookDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Observes the URL LiveData in [bookDetailViewModel] and updates the UI accordingly.
         * If the URL is not empty, sets the WebView's visibility to VISIBLE and loads the URL.
         * If the URL is empty, sets the WebView's visibility to GONE and shows a message indicating no data.
         *
         * @param viewLifecycleOwner The LifecycleOwner that controls the observer's lifecycle.
         */
        bookDetailViewModel.url.observe(viewLifecycleOwner) { url ->
            if (url.isNotEmpty()) {
                binding.webView.visibility = View.VISIBLE
                binding.noDataFoundTV.visibility = View.GONE
                binding.webView.loadUrl(url)
            }
            else {
                binding.webView.visibility = View.GONE
                binding.noDataFoundTV.visibility = View.VISIBLE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}