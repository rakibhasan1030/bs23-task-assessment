package com.rakibhasan.bs_assessment.feature.books.domain.usecase

import com.rakibhasan.bs_assessment.core.base.BaseResult
import com.rakibhasan.bs_assessment.core.base.Failure
import com.rakibhasan.bs_assessment.feature.books.data.remote.dto.BookResponseDto
import com.rakibhasan.bs_assessment.feature.books.domain.repository.BookRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BookUseCase @Inject constructor(
    private val bookRepository: BookRepository
) {
    suspend fun invokeBooks(): Flow<BaseResult<BookResponseDto, Failure>> = bookRepository.getBooks()
}