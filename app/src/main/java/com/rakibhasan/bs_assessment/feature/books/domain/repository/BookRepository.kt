package com.rakibhasan.bs_assessment.feature.books.domain.repository

import com.rakibhasan.bs_assessment.core.base.BaseResult
import com.rakibhasan.bs_assessment.core.base.Failure
import com.rakibhasan.bs_assessment.feature.books.data.remote.dto.BookResponseDto
import kotlinx.coroutines.flow.Flow


interface BookRepository {
    suspend fun getBooks(): Flow<BaseResult<BookResponseDto, Failure>>
}