package com.rakibhasan.bs_assessment.feature.books.data.remote.dto


import com.google.gson.annotations.SerializedName

data class BookResponseDto(
    @SerializedName("data") val books: List<Book>? = null,
    @SerializedName("message") val message: String? = null,
    @SerializedName("status") val status: Int? = null,
)