package com.rakibhasan.bs_assessment.feature.books.data.di

import com.rakibhasan.bs_assessment.feature.books.data.remote.BookRemoteSource
import com.rakibhasan.bs_assessment.feature.books.data.remote.repositoryImpl.BookRepositoryImpl
import com.rakibhasan.bs_assessment.feature.books.data.remote.api.BookApiService
import com.rakibhasan.bs_assessment.feature.books.domain.repository.BookRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object BookModule {
    @Provides
    @Singleton
    fun provideLoginApiService(retrofit: Retrofit): BookApiService {
        return retrofit.create(BookApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideLoginRemoteSource(apiServices: BookApiService): BookRemoteSource {
        return BookRemoteSource(apiServices)
    }

    @Provides
    @Singleton
    fun provideLoginRepository(bookRemoteSource: BookRemoteSource): BookRepository {
        return BookRepositoryImpl(bookRemoteSource)
    }
}