package com.rakibhasan.bs_assessment.feature.books.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rakibhasan.bs_assessment.core.base.BaseResult
import com.rakibhasan.bs_assessment.feature.books.data.remote.dto.BookResponseDto
import com.rakibhasan.bs_assessment.feature.books.domain.usecase.BookUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BooksViewModel @Inject constructor(private val bookUseCase: BookUseCase) : ViewModel() {

    private val _bookState = MutableStateFlow<BookState>(BookState.Init)
    val bookState: StateFlow<BookState> get() = _bookState

    private fun showLoading() {
        _bookState.value = BookState.IsLoading(true)
    }

    private fun hiddenLoading() {
        _bookState.value = BookState.IsLoading(false)
    }

    private fun showToast(message: String) {
        _bookState.value = BookState.ShowToast(message)
    }


    private val _books = MutableStateFlow(BookResponseDto())
    val books: StateFlow<BookResponseDto> get() = _books

    /**
     * Fetches books data asynchronously and updates the LiveData [_books] with the result.
     * Shows loading indicator while fetching data and handles errors by displaying a toast message.
     * Uses viewModelScope to launch the coroutine to ensure it's cancelled when ViewModel is cleared.
     */
    fun getBooks() {
        viewModelScope.launch {
            bookUseCase.invokeBooks()
                .onStart { showLoading() }
                .catch { e ->
                    hiddenLoading()
                    showToast(e.message.toString())
                }
                .collect { result ->
                    hiddenLoading()
                    when (result) {
                        is BaseResult.Success -> {
                            _books.value = result.data as BookResponseDto
                        }

                        is BaseResult.Error -> {
                            // 0 means no internet connection
                            if (result.error.code != 0) {
                                showToast(result.error.message)
                            }else{
                                showToast("No internet connection!")
                            }
                        }

                        else -> {}
                    }
                }
        }
    }

}

sealed class BookState {
    object Init : BookState()
    data class ShowToast(val message: String) : BookState()
    data class IsLoading(val isLoading: Boolean) : BookState()
}