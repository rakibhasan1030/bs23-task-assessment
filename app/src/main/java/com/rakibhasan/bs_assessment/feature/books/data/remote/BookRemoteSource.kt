package com.rakibhasan.bs_assessment.feature.books.data.remote

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rakibhasan.bs_assessment.core.base.BaseResult
import com.rakibhasan.bs_assessment.core.base.ErrorBodyResponse
import com.rakibhasan.bs_assessment.core.base.Failure
import com.rakibhasan.bs_assessment.core.exceptions.NoInternetConnectionException
import com.rakibhasan.bs_assessment.core.utils.Const
import com.rakibhasan.bs_assessment.feature.books.data.remote.api.BookApiService
import com.rakibhasan.bs_assessment.feature.books.data.remote.dto.BookResponseDto

class BookRemoteSource constructor(
    private var apiService: BookApiService
    ) {

    /**
     * Retrieves a list of books from the API service.
     *
     * @return A [BaseResult] representing the result of the operation. If successful, returns [BaseResult.Success] with the [BookResponseDto] data.
     *         If unsuccessful, returns [BaseResult.Error] with the [Failure] information.
     */
    suspend fun getBooks(): BaseResult<BookResponseDto, Failure> {
        val response = apiService.getBooks()
        try {
            return if (response.isSuccessful) {
                BaseResult.Success(response.body())
            } else {
                val type = object : TypeToken<ErrorBodyResponse>() {}.type
                val err: ErrorBodyResponse =
                    Gson().fromJson(response.errorBody()!!.charStream(), type)
                BaseResult.Error(Failure(response.code(), err.message!!))
            }
        } catch (e: NoInternetConnectionException) {
            return BaseResult.Error(Failure(0, e.message))
        } catch (e: Exception) {
            val type = object : TypeToken<ErrorBodyResponse>() {}.type
            val err: ErrorBodyResponse = Gson().fromJson(response.errorBody()!!.charStream(), type)
            return BaseResult.Error(Failure(-1, err.message!!))
        }

    }
}

