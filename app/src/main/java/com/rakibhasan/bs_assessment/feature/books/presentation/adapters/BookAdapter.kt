package com.rakibhasan.bs_assessment.feature.books.presentation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rakibhasan.bs_assessment.R
import com.rakibhasan.bs_assessment.databinding.ItemBookBinding
import com.rakibhasan.bs_assessment.feature.books.data.remote.dto.Book

class BooksAdapter(
    private var books: List<Book>,
    private var context: Context,
    private val onItemClick: (Book) -> Unit
) : RecyclerView.Adapter<BooksAdapter.ViewHolder>() {
    class ViewHolder(itemView: ItemBookBinding) : RecyclerView.ViewHolder(itemView.root) {
        var binding: ItemBookBinding? = null

        init {
            this.binding = itemView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ItemBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = books.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = books[position]
        holder.binding?.bookNameTv!!.text = item.name
        Glide
            .with(context)
            .load(item.image)
            .placeholder(R.drawable.placeholder_image)
            .into(holder.binding!!.bookImageIv)
        holder.binding?.learnMoreBtn?.setOnClickListener {
            onItemClick(item)
        }
    }

}
