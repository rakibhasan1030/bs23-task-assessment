package com.rakibhasan.bs_assessment.feature.books.data.remote.dto


import com.google.gson.annotations.SerializedName

data class Book(
    @SerializedName("id") val id: Int?,
    @SerializedName("image") val image: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("url") val url: String?
)