package com.rakibhasan.bs_assessment.feature.books.data.remote.api

import com.rakibhasan.bs_assessment.feature.books.data.remote.dto.BookResponseDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

interface BookApiService {
    @Headers("Accept: application/json")
    @GET("book-list")
    suspend fun getBooks(
    ): Response<BookResponseDto>
}