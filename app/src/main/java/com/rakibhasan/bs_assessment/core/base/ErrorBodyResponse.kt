package com.rakibhasan.bs_assessment.core.base

import com.google.gson.annotations.SerializedName

data class ErrorBodyResponse(
    @SerializedName("status") val status: Boolean? = null,
    @SerializedName("message") val message: String? = null,
)