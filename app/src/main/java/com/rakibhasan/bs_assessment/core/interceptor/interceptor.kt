package com.rakibhasan.bs_assessment.core.interceptor

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.rakibhasan.bs_assessment.core.interceptor.DataStorePref
import com.rakibhasan.bs_assessment.core.exceptions.NoInternetConnectionException
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response

class NetworkInterceptor constructor(val context: Context) : Interceptor {
    private var dataStorePref: DataStorePref = DataStorePref(context)

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = runBlocking {
            dataStorePref.getToken()
        }
        if (!isConnected())
            throw NoInternetConnectionException()

        val vals = chain.request().newBuilder().build()
        return chain.proceed(
            chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .method(chain.request().method, chain.request().body)
                .build()
        )
    }

    private fun isConnected(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            val newConnection = connectivityManager.activeNetwork ?: return false
            val activeNewConnection =
                connectivityManager.getNetworkCapabilities(newConnection) ?: return false

            return when {
                activeNewConnection.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNewConnection.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            val info = connectivityManager.activeNetworkInfo ?: return false
            return info.isConnected
        }
    }
}