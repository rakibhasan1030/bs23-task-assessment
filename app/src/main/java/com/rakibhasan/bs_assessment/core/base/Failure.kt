package com.rakibhasan.bs_assessment.core.base

import com.google.gson.annotations.SerializedName

data class Failure(
    @SerializedName("code") val code: Int,
    @SerializedName("message") val message: String,
)
