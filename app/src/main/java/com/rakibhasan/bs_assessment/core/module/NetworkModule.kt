package com.rakibhasan.bs_assessment.core.module

import android.content.Context
import com.rakibhasan.bs_assessment.BuildConfig
import com.rakibhasan.bs_assessment.core.interceptor.NetworkInterceptor
import com.rakibhasan.bs_assessment.core.utils.Const
import com.rakibhasan.bs_assessment.core.utils.Const.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit{
        return Retrofit.Builder().apply {
            baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
        }.build()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient (networkInterceptor: NetworkInterceptor): OkHttpClient{
        return OkHttpClient.Builder().apply {
            addInterceptor(networkInterceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
        }.build()
    }

    @Provides
    fun provideInterceptor (@ApplicationContext context: Context): NetworkInterceptor {
        return NetworkInterceptor(context)
    }
}