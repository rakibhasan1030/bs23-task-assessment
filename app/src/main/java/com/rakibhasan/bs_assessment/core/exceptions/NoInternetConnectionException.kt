package com.rakibhasan.bs_assessment.core.exceptions

import okio.IOException


class NoInternetConnectionException : IOException() {
    override val message: String
        get() = "No internet Connection! "
}