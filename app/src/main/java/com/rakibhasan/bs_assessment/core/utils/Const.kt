package com.rakibhasan.bs_assessment.core.utils

import android.app.Activity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.navigation.NavOptions
import com.rakibhasan.bs_assessment.R

object Const {
    const val BASE_URL = "https://example.com/api"

    // navigation transaction animation constants with destination
    fun getNavOptions(popUpToId: Int = 0, isInclusive: Boolean = false): NavOptions =
        NavOptions.Builder()
            .setEnterAnim(R.anim.enter_from_right)
            .setExitAnim(R.anim.exit_to_left)
            .setPopEnterAnim(R.anim.enter_from_left)
            .setPopExitAnim(R.anim.exit_to_right)
            .apply { if (popUpToId != 0) setPopUpTo(popUpToId, isInclusive) }
            .build()

    fun showKeyboard(activity: Activity) =
        WindowCompat.getInsetsController(activity.window, activity.window.decorView)
            .show(WindowInsetsCompat.Type.ime())

    fun closeKeyboard(activity: Activity) =
        WindowCompat.getInsetsController(activity.window, activity.window.decorView)
            .hide(WindowInsetsCompat.Type.ime())
}
