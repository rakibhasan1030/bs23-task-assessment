package com.rakibhasan.bs_assessment.core.interceptor

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "filter")

class DataStorePref(context: Context) {
    private val dataStore: DataStore<Preferences> = context.dataStore

    companion object {
        val NAME = stringPreferencesKey("name")
        val EMAIL = stringPreferencesKey("email")
        val PHONE = stringPreferencesKey("phone")
        val ADDRESS = stringPreferencesKey("address")
        val IMAGE = stringPreferencesKey("image")
        val STATUS = booleanPreferencesKey("status")
        val TOKEN = stringPreferencesKey("token")
        val TOKEN_TYPE = stringPreferencesKey("tokenType")
    }

    suspend fun saveData(
        name: String,
        email: String,
        phone: String,
        address: String,
        image: String,
        status: Boolean,
        token: String,
        tokenType: String,

        ) {
        dataStore.edit {
            it[NAME] = name
            it[EMAIL] = email
            it[PHONE] = phone
            it[ADDRESS] = address
            it[IMAGE] = image
            it[STATUS] = status
            it[TOKEN] = token
            it[TOKEN_TYPE] = tokenType
        }
    }

    suspend fun clearData() {
        dataStore.edit {
            it.clear()
        }
    }


    /*    val nameFlow: Flow<String> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it[NAME] ?: ""
            }*/
    suspend fun getName(): String = dataStore.data.first()[NAME] ?: ""


    /*    val emailFlow: Flow<String> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it[EMAIL] ?: ""
            }*/

    suspend fun getEmail(): String = dataStore.data.first()[EMAIL] ?: ""


    /*    val addressFlow: Flow<String> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it[ADDRESS] ?: ""
            }*/

    suspend fun getAddress(): String = dataStore.data.first()[ADDRESS] ?: ""


    /*    val imageFlow: Flow<String> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it[IMAGE] ?: ""
            }*/

    suspend fun getImage(): String = dataStore.data.first()[IMAGE] ?: ""

    /*    val phoneFlow: Flow<String> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it[PHONE] ?: ""
            }*/

    suspend fun getPhone(): String = dataStore.data.first()[PHONE] ?: ""


    /*    val statusFlow: Flow<Boolean> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it[STATUS] ?: false
            }*/

    suspend fun getStatus(): Boolean = dataStore.data.first()[STATUS] ?: false

    /*    val tokenFlow: Flow<String> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it[TOKEN] ?: ""
            }*/

    suspend fun getToken(): String = dataStore.data.first()[TOKEN] ?: ""

    /*    val tokenTypeFlow: Flow<String> = dataStore.data
            .catch { exception ->
                if (exception is IOException) {
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map {
                it[TOKEN_TYPE] ?: ""
            }*/
    suspend fun getTokenType(): String = dataStore.data.first()[TOKEN_TYPE] ?: ""


}